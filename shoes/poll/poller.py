import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
from shoes_rest.models import BinVO
# from shoes_rest.models import Something

def get_bin():
    #gets bins from wardrobe api
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    #decodes json string response
    content = json.loads(response.content)
    print(content)
    #loops throough the decoded response
    for bin in content['bins']:
        #for each bin will update BinVO data if existing or create BinVO object if new
        BinVO.objects.update_or_create(
            #assigns binVO variable to the corresponding object value
            import_href = bin['href'],
            defaults = {'closet_name': bin['closet_name']}
        ) #this could also be written into the try block of poll instead of creating this whole other function

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bin() #calls the get bin function
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

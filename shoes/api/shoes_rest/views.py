from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO

#=====================Encoders==========================

class BinVOEncoder(ModelEncoder): #set properties of BinVO instance to JSON serializable object
    model= BinVO
    properties= [
        'id',
        'import_href',
        'closet_name',
    ]

class ShoeDetailEncoder(ModelEncoder): #sets expected keys for values to be submitted on creation/update
    model = Shoe
    properties = [
        'id',
        'manufacturer',
        'make',
        'color',
        'bin',
        'pic_url'
    ]
    encoders = {
        "bin": BinVOEncoder() #use bin encoder to set bin property to Json serializable object
    }


#===================Views=====================

@require_http_methods(['GET', 'POST'])
def api_list_shoes(request):
    if request.method == 'GET':
        shoes = Shoe.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder = ShoeDetailEncoder,
            safe=False,
        )  
    else:
        content = json.loads(request.body) #decodes json string of keys:values submitted
        
        try:
            bin = BinVO.objects.get(import_href=content['bin'])#retrieves the desired binVO
            content['bin'] = bin #replaces the request body data with the desired binVO
            
        except BinVO.DoesNotExist:
            return JsonResponse({'message': 'Bin does not exist'}, status = 400)
            
        shoe = Shoe.objects.create(**content) #creates an instance of shoe related to desired binVO
        return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_shoe_detail(request, id):
    if request.method == 'GET':
        shoe = Shoe.objects.filter(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == 'DELETE':
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({'DELETED': count > 0})
    else:
        content = json.loads(request.body)
        try:
            shoe = Shoe.objects.filter(id=id).update(**content)
            updated_shoe = Shoe.objects.filter(id=id)
            return JsonResponse(
                updated_shoe,
                encoder = ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {'Error': 'Invalid Instance'}
            )
            
        


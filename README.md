# Wardrobify

Team:

* Person 1 - Krystin, shoes
* Person 2 - -, bonus hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

- Create and use poller to query bins in wardrobe at interval, then create or update BinVO objects.
- BinVO is my model for the  bin data that I want to use.
- Shoe is my model for data to be taken for each shoe instance.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

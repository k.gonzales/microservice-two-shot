import React, { useState, useEffect } from "react";

const CreateNewShoe = function (){
    const [make, setMake] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('')
    const [picUrl, setPicUrl] = useState('');
    const [bins, setBins] = useState([]);
    const [binChoice, setBinChoice] = useState('');

    const handleMake = (event) => {
        const value = event.target.value;
        setMake(value);
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePicUrl = (event) => {
        const value  = event.target.value;
        setPicUrl(value);
    }

    const fetchData = async () =>{
        const url= 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(()=>{
        fetchData();
    }, []);

    const handleBinChoice = (event) => {
        const value = event.target.value;
        setBinChoice(value);
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data={};
        data.make = make;
        data.manufacturer = manufacturer;
        data.color = color;
        data.pic_url = picUrl;
        data.bin = binChoice;

        console.log(data);

        const url = 'http://localhost:8080/api/shoes/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        
        const postResponse = await fetch(url, fetchOptions);
        if (postResponse.ok) {
            setMake('');
            setManufacturer('');
            setColor('');
            setPicUrl('');
            setBinChoice('');
        }
    }

    return(
        <div className="row" >
            <div className="offset-3 col-6" >
                <div className="shadow p-4 mt-4 mb-4" style={{backgroundColor: "gray"}}>
                    <form id="create-shoe-form" onSubmit={handleSubmit} >
                        <h1>Create your new shoe!</h1>
                        <div className="form-group mb-3">
                            <label htmlFor="make">Model Name</label>
                            <input 
                            onChange={handleMake} 
                            value={make} 
                            type="text" 
                            id="make" 
                            className="form-control" 
                            required/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="manufacturer">Manufacturer</label>
                            <input 
                            onChange={handleManufacturer}
                            value={manufacturer}
                            type="text" 
                            id="manufacturer" 
                            className="form-control" 
                            required/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="color">Color</label>
                            <input 
                            onChange={handleColor}
                            value={color}
                            type="text" 
                            id="color" 
                            className="form-control" 
                            required/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="pic_url">Picture URL(optional)</label>
                            <input 
                            onChange={handlePicUrl}
                            value={picUrl}
                            type="url" 
                            id="pic_url" 
                            className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <select 
                            onChange={handleBinChoice}
                            value = {binChoice}
                            required 
                            className="form-select">
                                <option value="">Select a bin</option>
                                {bins.map(bin =>{
                                    return(
                                        <option key={bin.href} value={bin.href}>{bin.closet_name} - bin: {bin.bin_number}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default CreateNewShoe;
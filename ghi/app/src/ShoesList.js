import React, { useState, useEffect } from 'react';
import ShoeCard from './ShoeCard';

const ShoesList = function(props){
    const [shoes, setShoes] = useState([]);

    const getShoeList = async function() {
        const url='http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok){
            const shoes= await response.json();
            const shoedata = [];
            for (let shoe of shoes.shoes) {
                shoedata.push(shoe);
            }
            setShoes(shoedata);
        }
    }

    const deleteShoe = async function(id) {
        const url = `http://localhost:8080/api/shoes/${id}/`;
        try{
            const response = await fetch(url,{method: 'delete'});
            if (response.ok){
                const updated = shoes.filter(shoe => shoe.id !== id);
                setShoes(updated);
            }
        }
        catch(error) {
            console.log('error', error);
        }
    }
    
    useEffect(() => {
        getShoeList();
    }, []);

    return(
        <div className='container'>
            <h1 style={{
                textAlign: 'center', 
                textDecoration: 'underline'
                }}>List of all Shoes</h1>
            <div className='mb-2'>
                <a type='button' className='btn btn-success btn-md w-25' href='/shoes/new'>Add a Shoe</a>
            </div>
            <div className='row row-cols-1 row-cols-md-3'>
                {shoes.map(shoe => <ShoeCard key={shoe.id} shoe={shoe} deleteShoe={deleteShoe} />)}
            </div>
        </div>
    );
}
export default ShoesList;

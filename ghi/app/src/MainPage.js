function MainPage() {
  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <h5 className="mb-4">
            Need to keep track of your shoes and hats? We have
            the solution for you!
          </h5>
          <img src='https://images.pexels.com/photos/3315286/pexels-photo-3315286.jpeg' className='img-thumbnail'></img>
        </div>
      </div>
    </>
  );
}

export default MainPage;

import React from 'react';

const ShoeCard = function({shoe, deleteShoe}) {
    return(
        
            <div className='col mb-4'>
                <div className='card h-100 shadow' >
                    <img src={shoe.pic_url} className="card-img-top img-thumbnail"/>
                    <div className="card-body">
                        <h5 className="card-title">{shoe.make}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{shoe.manufacturer}</h6>
                        <p className="card-text">{shoe.color}</p>
                        <p className="card-text">{shoe.bin.closet_name}, bin {shoe.bin.id}</p>
                    </div>
                    <div className='card-footer'>
                        <button className='btn btn-danger rounded w-100' type='button' onClick={()=>deleteShoe(shoe.id)} >Delete</button>
                    </div>
                </div>
            </div>
        
    );
}

export default ShoeCard;